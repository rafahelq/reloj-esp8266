# Reloj ESP8266

Crea tu propio reloj IOT con una placa [ESP8266](https://es.aliexpress.com/item/32647690484.html?spm=a2g0s.9042311.0.0.7de363c0PurV1W) y una matrix 4 en 1 [MAX7219](https://es.aliexpress.com/item/32618155357.html?gps-id=detail404&scm=1007.16891.96945.0&scm_id=1007.16891.96945.0&scm-url=1007.16891.96945.0&pvid=9ce4d887-96b0-4e26-af68-52af13e36223).
Conexión wifi para recoger la hora (Google) y el tiempo (OpenWeather).

<a rel="Reloj ESP8266" href="https://gitlab.com/podcastlinux/reloj-esp8266/blob/master/IMG_20200101_200948.jpg">
<img alt="Reloj ESP8266" style="border-width:0" src="https://gitlab.com/podcastlinux/reloj-esp8266/raw/master/IMG_20200101_200948.jpg" />
</a>


[Vídeo](https://twitter.com/podcastlinux/status/1212994942693445633) en Twitter.

**Características:**
+ Código a través de Arduino IDE.
+ Hora configurando tu zona horaria.
+ Fecha en varios formatos.
+ Cambio de hora en horario de verano.
+ Datos meteorológicos desde tu ciudad.
+ Elige los datos meteorológicos que desees.
+ Texto personalizable opcional.
+ Todos los datos están traducidos al español con tíldes.

**Mejoras:**
+ Diseño carcasa 3D. 
+ Configuración a través de web (ip).
+ Poder añadir alarmas.
+ Añadir más datos: Twitter, Youtube, ....

**Configuración Arduino IDE:**
+ Compatibilidad con ESP8266: Preferencias > Ajustes > Gestor de URLs Adicionales de tarjetas: http://arduino.esp8266.com/stable/package_esp8266com_index.json.
+ Herramientas > Placas > NodeMCU 1.0 (ESP-12E Module).
+ Librerías: Adafruit_GFX.h, Max72xxPanel.h y ArduinoJson.h.

Anímate a colaborar y mejorar este proyecto: Eres bienvenido/a.  
Si tienes algún problema, ponte en contacto conmigo.

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  